# Materia: Programacion Paralela (ICA)
# Desarrollador: Ramon Ramos
# Cedula: 15281565
# Tarea 1 - Crear de un programa con MPI que cuente la cantidad de palabras en un archivo texto.

from mpi4py import MPI
import os
import operator
import sys

#funcion de reduccion 

def reduccion_dic(a,b):
	for aux in b.iteritems():
		valor=a.get(aux[0])
		if(valor):
			a[aux[0]]=a[aux[0]]+aux[1]
		else:
			a.update({aux[0]:aux[1]})
	return a
	

comm = MPI.COMM_WORLD
rango = comm.Get_rank()
mundo_tamano=comm.Get_size()

#buscamos el tamano del archivo y el bloque que se va a leer
if(len(sys.argv)==2 and os.path.isfile(sys.argv[1])):
	archivo=sys.argv[1]
	tamano=os.path.getsize(archivo)
	bloque=tamano/mundo_tamano
	inicio=bloque*(rango)
	#abrimos el archivo y nos colocamos donde queremos empezar a leer
	fp=open(archivo,'r')
	t1=0
	t2=0
	if(fp):
		#me coloco en el inicio de la linea
		final=inicio+bloque
		if(rango!=0):
			fp.seek(inicio-1)
			if(fp.read(1)=='\n'):
				fp.seek(inicio)
			else:
				fp.readline()
				inicio=fp.tell()
		else:
			t1=MPI.Wtime()
		posicion=inicio

		print "procesador "+str((rango+1))+" i="+str(posicion)+" f="+str(final)	
		tabla={'&':-1}
		linea='&'
		while (posicion<final):
			linea=fp.readline()
			posicion=fp.tell()
			#quito los caracteres especiales
			linea=linea.replace('.','')
			linea=linea.replace('!','')
			linea=linea.replace('#','')
			linea=linea.replace(',','')
			linea=linea.replace(':','')
			linea=linea.replace('\n','')
			linea=linea.replace('-','')
			linea=linea.replace('+','')
			linea=linea.replace('*','')
			linea=linea.replace('=','')
			linea=linea.lower()
			#divido la cadena en palabras.
			palabras=linea.split(' ')
			for palabra in palabras:
				if(palabra and len(palabra)>1):
					valor=tabla.get(palabra)
					if(valor):
						tabla[palabra]=tabla[palabra]+1
					else:
						aux={palabra:1}
						tabla.update(aux)
		
		#se ejecuta un reduce para sumar todos los diccionarios
		tabla_x=comm.reduce(tabla,op=reduccion_dic,root=0)
	
		if(rango==0):
			t2=MPI.Wtime()
			ordenada=sorted(tabla_x.iteritems(),key=operator.itemgetter(1),reverse=True)
			for i in range(0,19):
				print ordenada[i]
			t3=t2-t1
			print "El proceso se tardo "+str(t3)+ " con "+ str(mundo_tamano) +" Procesador"
	fp.close()
else:
	print "Coloque como parametro un archivo texto a analizar valido"

